/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author juras
 */
public class simPanel extends JPanel{


    @Override
    public void paint(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
                int blockSize =Math.min(getWidth()/World.width, getHeight()/World.height)-2;

            
        for (int x = 0; x < World.width; x++) {
            for (int y = 0; y < World.height;y++) {
                
                Agent Boris = World.grid[x][y];
                if (Boris==null) {
                    g.setColor(Color.GRAY);
                } else if (Boris.toString() == "Rabbit") {
                    g.setColor(Color.GREEN);
                }else if (Boris.toString() == "Fox") {
                    g.setColor(Color.ORANGE);
                }else if (Boris.toString() == "Hunter") {
                    g.setColor(Color.WHITE);
                }else if (Boris.toString() == "Arrow") {
                    g.setColor(Color.YELLOW);
                }else if (Boris.toString() == "Edge") {
                    g.setColor(Color.BLACK);
                }  else {
                    g.setColor(Color.BLUE);
                }

                g.fillRect((blockSize + 1) * x + 1, (blockSize + 1) * y + 1, blockSize, blockSize);
                
                g.setColor(Color.CYAN);
                g.drawString("" , (blockSize + 1) * x -3 + blockSize/2, (blockSize + 1) * y + 5 + blockSize / 2);
               
            }
        }
    }
    
    
}

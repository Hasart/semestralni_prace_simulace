/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * World is one of most important clases - it contain simulator grid.
 *
 * @author Miroslav
 */
public class World {

    static int width;
    static int height;
    static int speedOfWorld = 1;

    Gui gui;
    Random rand = new Random();

    ArrayList<Rabbit> rabbitList = new ArrayList<>();
    ArrayList<Fox> foxList = new ArrayList<>();

    static Agent[][] grid;

    public World(int width, int height, Gui gui) {
        World.width = width;
        World.height = height;
        this.gui = gui;

        makeFields();
        guiThread();
        gui.setWorld(this);

    }

    /**
     * Make empty grid with wall around.
     */
    private void makeFields() {
        grid = new Agent[width][height];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                grid[j][i] = null;

                if (i == 0 || j == 0 || i == height - 1 || j == width - 1) {

                    grid[j][i] = new Edge();
                }
            }
        }
    }

    /**
     * This method returns an agent who is in coordinates x, y.
     *
     * @param x
     * @param y
     * @return
     */
    public Agent getAgent(int x, int y) {
        return grid[x][y];
    }

    /**
     * This method adds a Rabbit into the grid on random position.
     *
     * @return
     */
    public Rabbit addRabit() {
        Rabbit temp = null;
        while (true) {
            int rx = rand.nextInt(width);
            int ry = rand.nextInt(height);
            if (grid[rx][ry] == null) {
                temp = new Rabbit(rx, ry);
                grid[rx][ry] = temp;
                rabbitList.add(temp);
                break;
            }
        }
        return temp;
    }

    /**
     * This method adds a Fox into the grid on random position.
     *
     * @return
     */
    public Fox addFox() {
        Fox temp = null;
        while (true) {
            int rx = rand.nextInt(width);
            int ry = rand.nextInt(height);
            if (grid[rx][ry] == null) {
                temp = new Fox(rx, ry);
                grid[rx][ry] = temp;
                foxList.add(temp);
                break;
            }
        }
        return temp;
    }

    /**
     * This method adds a Rabbit into the grid on random position and start him
     * in a new thread.
     */
    public void addRabitInThread() {
        new Thread(addRabit()).start();
    }

    /**
     * This method adds a Fox into the grid on random position and start him in
     * a new thread.
     */
    public void addFoxInThread() {
        new Thread(addFox()).start();
    }

    /**
     * This method can add multiple Rabbits into the grid on random position.
     *
     * @param num number of Rabbits
     */
    public void addRabits(int num) {
        int NumOfRabbits = num;
        while (NumOfRabbits != 0) {
            int rx = rand.nextInt(width);
            int ry = rand.nextInt(height);
            if (grid[rx][ry] == null) {
                Rabbit temp = new Rabbit(rx, ry);
                grid[rx][ry] = temp;
                rabbitList.add(temp);
                NumOfRabbits--;

            }
        }
    }

    /**
     * This method can add multiple Foxes into the grid on random position.
     *
     * @param num number of Foxes
     */
    public void addFoxes(int num) {
        int NumOfFoxes = num;
        while (NumOfFoxes != 0) {
            int rx = rand.nextInt(width);
            int ry = rand.nextInt(height);
            if (grid[rx][ry] == null) {
                Fox temp = new Fox(rx, ry);
                grid[rx][ry] = temp;
                foxList.add(temp);
                NumOfFoxes--;
            }
        }
    }

    private void getGui(Gui gui) {
        this.gui = gui;
    }

    private void guiThread() {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
                }

                gui.repaint();
            }
        }).start();
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static void setSpeedOfWorld(float speedOfWorld) {
        World.speedOfWorld = (int) speedOfWorld;
    }

    /**
     * This method kills all agents in the grid.
     */
    public void clearField() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (getAgent(j, i) != null) {
                    Agent temp = grid[j][i];
                    temp.isAlive = false;

                }

            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is an abstract class for all objects in te grid.
 *
 * @author juras
 */
public abstract class Agent implements Runnable {

    int x, y;
    boolean isAlive = true;
    boolean isReadyToMakeOut = false;
    int daysWithoutChild = 0;
    int childPause;
    int energy;
    int score;
    int maxSence;
    int minReactionTime;
    int maxReactTime;
    Random rand;

    public Agent() {
        this.rand = new SecureRandom();

    }

    @Override
    public void run() {
        while (isAlive) {
            daysWithoutChild++;
            int roundInMilis = rand.nextInt(maxReactTime - minReactionTime);
            if (daysWithoutChild > childPause) {
                isReadyToMakeOut = true;
            }

            scanSurraunds(x, y, this);
            try {
                Thread.sleep(roundInMilis / (World.speedOfWorld));

            } catch (InterruptedException ex) {
                Logger.getLogger(Rabbit.class.getName()).log(Level.SEVERE, null, ex);
            }
            energy--;
            if (energy <= 0) {
                isAlive = false;
            }
        }
        try {
            Thread.sleep(2000/World.speedOfWorld); // wait a while and than remove corpses
        } catch (InterruptedException ex) {
            Logger.getLogger(Agent.class.getName()).log(Level.SEVERE, null, ex);
        }
        World.grid[x][y] = null;

    }

    /**
     * This method move agent to random field nearby. SouthEast=1 noMove=5
     * NorthWest=9
     *
     * @param x original x coordinat
     * @param y original y coordinat
     * @param Smith agent
     * @param direction SouthEast=1 noMove=5 NorthWest=9
     */
    protected void move(int x, int y, Agent Smith, int direction) {
        int resultX = x;
        int resultY = y;
        switch (direction) {
            case 5:
                resultX++;
                resultY++;
                break;
            case 1:
                resultY++;
                break;
            case 3:
                resultX--;
                resultY++;
                break;
            case 4:
                resultX++;
                break;
            case 2:
                break;
            case 6:
                resultX--;
                break;
            case 7:
                resultX++;
                resultY--;
                break;
            case 8:
                resultY--;
                break;
            case 0:
                resultX--;
                resultY--;
                break;
            default:
                break;

        }
        if (World.grid[resultX][resultY] == null) {
            World.grid[x][y] = null;
            this.x = resultX;
            this.y = resultY;
            World.grid[resultX][resultY] = Smith;
        }

    }

    /**
     * Scan surroundings(up to maxsence) of agent Smith and make his
     * action(replicate, eat or move).
     *
     * @param x
     * @param y
     * @param smith
     */
    protected void scanSurraunds(int x, int y, Agent smith) {
        boolean tempStat = false;
        mainForCyklus:
        for (int i = 1; i <= maxSence; i++) {
            int tempX = x;
            int tempY = y;

            tempX += i;
            tempY += i;
            for (int j = 0; j <= (2 * i); j++) {
                tempY--;

                tempStat = makeAction(tempX, tempY, smith);
                if (tempStat) {
                    break mainForCyklus;
                }
            }
            for (int j = 0; j <= (2 * i); j++) {
                tempX--;
                tempStat = makeAction(tempX, tempY,  smith);
                if (tempStat) {
                    break mainForCyklus;
                }
            }
            for (int j = 0; j <= (2 * i); j++) {
                tempY++;
                tempStat = makeAction(tempX, tempY,  smith);
                if (tempStat) {
                    break mainForCyklus;
                }
            }
            for (int j = 0; j <= (2 * i); j++) {
                tempX++;
                tempStat = makeAction(tempX, tempY,  smith);
                if (tempStat) {
                    break mainForCyklus;
                }
            }
        }
        if (!tempStat) {
            move(x, y, smith, rand.nextInt(9));
        }

    }

    private boolean makeAction(int tempX, int tempY, Agent smith) {
        boolean status = false;
        Agent temp;

        try {
            temp = World.grid[tempX][tempY];
        } catch (ArrayIndexOutOfBoundsException e) {
         Logger.getLogger(Agent.class.getName()).log(Level.INFO, "Agent tried to jump out of grid. - caught ", e);

            return status;
        }

        if (temp != null && temp.isReadyToMakeOut && smith.isReadyToMakeOut && "Rabbit".equals(temp.toString()) && temp.toString().equals(smith.toString())) {
            birthNewAgent(this.x, this.y, "Rabbit");
            status = true;
        }
        if (temp != null && temp.isReadyToMakeOut && smith.isReadyToMakeOut && "Fox".equals(temp.toString()) && temp.toString().equals(smith.toString())) {
            birthNewAgent(this.x, this.y, "Fox");
            status = true;
        } else if ("Fox".equals(smith.toString()) && temp != null && "Rabbit".equals(temp.toString())) {
            smith.eatRabbit((Rabbit) temp);
            status = true;
        }
        return status;
    }

    /**
     *If there are two animals same species, there will be new one around.
     * @param x
     * @param y
     * @param agent
     */
    protected void birthNewAgent(int x, int y, String agent) {
        if (isReadyToMakeOut) {
            int resultX = x;
            int resultY = y;
            switch (rand.nextInt(8)) {
                case 5:
                    resultX++;
                    resultY++;
                    break;
                case 1:
                    resultY++;
                    break;
                case 3:
                    resultX--;
                    resultY++;
                    break;
                case 4:
                    resultX++;
                    break;
                case 2:
                    break;
                case 6:
                    resultX--;
                    break;
                case 7:
                    resultX++;
                    resultY--;
                    break;
                case 8:
                    resultY--;
                    break;
                case 0:
                    resultX--;
                    resultY--;
                    break;
                default:
                    break;

            }
            for (int i = 0; i < 10; i++) {

                {

                    if (World.grid[resultX][resultY] == null) {
                        Agent child = null;
                        if ("Rabbit".equals(agent)) {
                            child = new Rabbit(resultX, resultY);
                        } else if ("Fox".equals(agent)) {
                            child = new Fox(resultX, resultY);
                        }

                        World.grid[resultX][resultY] = child;

                        new Thread(child).start();
                        isReadyToMakeOut = false;
                        break;

                    }
                }
            }
        }
    }

    private void eatRabbit(Rabbit rabbit) {
        energy += 5;
        rabbit.isAlive = false;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

/**
 *
 * @author juras
 */
public class Rabbit extends Agent {

    public Rabbit(int x, int y) {
        minReactionTime = 500;
        maxReactTime = 1500;
        maxSence = 1;
        this.x = x;
        this.y = y;
        isReadyToMakeOut = false;
        childPause = 3;
        energy = 10;
    }

    @Override
    public String toString() {
        if (!isAlive) {
            return " 0 ";
        }
        return "Rabbit";
    }

}

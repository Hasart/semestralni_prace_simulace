/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.GridLayout;
import javax.swing.event.ChangeEvent;

/**
 * Main graphic window.
 *
 * @author juras
 */
public class Gui extends JFrame {

    transient World world;

    public Gui() {
        super("Simulation");

        init();
    }
    private javax.swing.JButton addRabbit;
    private javax.swing.JButton addFox;
    private javax.swing.JButton clear;
    private javax.swing.JPanel simPanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JSpinner jSpinner1;

    private void init() {

        setTitle("Simulation");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 800);
        setResizable(false);
        setLayout(new BorderLayout());

        controlPanel = new javax.swing.JPanel(new GridLayout(1, 4));
        addRabbit = new javax.swing.JButton("Add Rabbit");
        addFox = new javax.swing.JButton("Add Fox");
        clear = new javax.swing.JButton("Clear");

        SpinnerModel model = new SpinnerNumberModel(1, 1, 100, 1);
        jSpinner1 = new javax.swing.JSpinner(model);

        controlPanel.add(addRabbit);
        controlPanel.add(addFox);
        controlPanel.add(clear);
        controlPanel.add(jSpinner1);

        add(controlPanel, BorderLayout.SOUTH);

        simPanel = new simPanel();
        add(simPanel, BorderLayout.CENTER);
        simPanel.setFocusable(true);

        addRabbit.addActionListener((java.awt.event.ActionEvent evt) -> {
            addRabbitActionPerformed(evt);
        });
        addFox.addActionListener((java.awt.event.ActionEvent evt) -> {
            addFoxActionPerformed(evt);
        });
        clear.addActionListener((java.awt.event.ActionEvent evt) -> {
            clearActionPerformed(evt);
        });

        jSpinner1.addChangeListener((ChangeEvent e) -> {
            World.setSpeedOfWorld((int) jSpinner1.getValue());
        });
    }

    private void addRabbitActionPerformed(java.awt.event.ActionEvent evt) {
        world.addRabitInThread();
    }

    private void addFoxActionPerformed(java.awt.event.ActionEvent evt) {
        world.addFoxInThread();
    }

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {
        world.clearField();
    }

    public void setWorld(World world) {
        this.world = world;
    }
}

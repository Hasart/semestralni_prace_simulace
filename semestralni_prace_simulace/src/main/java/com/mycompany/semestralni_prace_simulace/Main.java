/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

/**
 *
 * @author juras
 */
public class Main {

    public Main() {
    }

    /**
     * Creates new gui and new world.
     *
     * @param args width,height,numOfAnimals
     */
    public static void main(String[] args) {
        final int width;
        final int height;
        final int numOfAnimals;
        if (args.length == 3) {
            width = Integer.parseInt(args[0]);
            height = Integer.parseInt(args[1]);
            numOfAnimals = Integer.parseInt(args[2]);

        } else { //default parametres
            width = 40;
            height = 40;
            numOfAnimals = 20;
        }

        Gui gui = new Gui();
        World world = new World(width, height, gui);
        gui.setVisible(true);
        for (int i = 0; i < numOfAnimals; i++) {
            world.addRabitInThread();
            world.addFoxInThread();
        }

    }

}

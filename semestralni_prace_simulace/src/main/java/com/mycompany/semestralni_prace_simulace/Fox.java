/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semestralni_prace_simulace;

/**
 *
 * @author juras
 */
public class Fox extends Agent {

    public Fox(int x, int y) {
        minReactionTime = 1000;
        maxReactTime = 3000;
        energy = 5;
        this.x = x;
        this.y = y;

        maxSence = 1;
        isReadyToMakeOut = false;
        childPause = 5;
    }

    @Override
    public String toString() {
        if (!isAlive) {
            return " 0 ";
        }
        return "Fox";
    }

}
